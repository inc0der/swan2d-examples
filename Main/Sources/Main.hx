package;

import swan.Game;
import scenes.BootScene;

class Main {
	public static function main() {
		var bootScene = new BootScene();
		new Game({
			title: 'swan2d - Example Game',
			width: 720,
			height: 600,
			scenes: [bootScene]
		});
	}
}
