package entities;

import kha.math.Vector2;
import swan.objects.Sprite;
import swan.resources.Texture;

class Benchy extends Sprite {
	public var movingLeft: Bool = false;
	public var movingRight: Bool = true;
	public var movingUp: Bool = false;
	public var movingDown: Bool = true;
	public var speed: Int = 100;

	public function new(texture: Texture, position: Vector2, ?width: Int, ?height: Int) {
		super(texture, position, width, height);
	}

	override public function update() {
		if (x >= 700) {
			movingRight = false;
			movingLeft = true;
		} else if (x <= 0) {
			movingLeft = false;
			movingRight = true;
		} else if (y >= 580) {
			movingUp = false;
			movingDown = true;
		} else if (y <= 0) {
			movingUp = true;
			movingDown = false;
		}
	}
}
