package scenes;

import kha.audio1.Audio as KhaAudio;
import swan.resources.Audio;
import swan.objects.Sprite;
import swan.resources.Texture;
import swan.Scene;
import kha.math.Vector2;
import entities.Benchy;

class BootScene extends Scene {
	public var benchy: Benchy;
	public var characters: Array<Benchy> = [];
	public var background: Sprite;

	override public function new() {
		super('BootScene');
	}

	override public function preload() {
		super.preload();
		game.resource.loadAudio('universe-at-hand.ogg');
		game.resource.loadTexture('logo.png');
		game.resource.loadTexture('inc0der.png');
	}

	function addBackground() {
		var texture: Texture = game.resource.texture('logo.png');
		var width: Int = Math.round(texture.width / 2);
		var height: Int = Math.round(texture.height / 2);
		var background = new Sprite(texture, new Vector2(0, 0), width, height);
		this.add(background);
	}

	function addBenchy() {
		var texture: Texture = game.resource.texture('inc0der.png');
		benchy = new Benchy(texture, new Vector2(0, 0));
		this.add(benchy);
	}

	function addBackgroundMusic() {
		var audioResource: Audio = game.resource.audio('universe-at-hand.ogg');
		KhaAudio.play(audioResource.source);
	}

	override public function create() {
		addBackgroundMusic();
		addBackground();
		addBenchy();
	}

	override public function update(delta: Float) {
		if (!isReady) {
			return;
		}
		super.update(delta);
		if (benchy != null) {
			var speed = benchy.speed * delta;
			if (benchy.movingRight) {
				benchy.x += speed;
			} else if (benchy.movingLeft) {
				benchy.x -= speed;
			}
			// if (benchy.movingDown) {
			// 	benchy.y -= speed;
			// } else if (benchy.movingUp) {
			// 	benchy.y += speed;
			// }
		}
		// for (character in characters) {
		// 	if (character.x > 1200 || character.x < 0 && character.y > 700 || character.y < 0) {
		// 		return;
		// 	}
		// 	if (character.movingRight) {
		// 		character.x += character.speed * delta;
		// 	} else if (character.movingLeft) {
		// 		character.x -= character.speed * delta;
		// 	}
		// 	if (character.movingDown) {
		// 		character.y += character.speed * delta;
		// 	} else if (character.movingUp) {
		// 		character.y -= character.speed * delta;
		// 	}
		// }
	}
}
